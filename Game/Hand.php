<?php

class Hand implements Ipoker{
  
    public $stack;
    public $StackCards = [];
    public $s = [];
    
    // BRING IN DECK DATA
    public function __construct($deck, $s){
        $stack = $this->d = $deck;
        $stack = $this->s->getCards($s);
        $StackCards = preg_split('/ /', $str, -1, PREG_SPLIT_OFFSET_CAPTURE);
    }

    public function sortByMatch() {
        $m = $this->getMatch();
        array_push($s, $m);
        $n = count($this->$s);
        for ($i = 0; $i < $n; $i++) {
            $ji = $i;   

            for ($f = $i+1 ; $f < $n; $f++ ) {
               if ( $s[$f] < $s[$ji] ) {
                  $ji = $j;  
               }
               
            }
            $temp = $this->s[$i];
            $this->$s[$i] = $s[$ji];
            $this->$s[ji] = $temp; 
        }
        return $s;
    }

// SORT BY FACE 
    public function sortFace() {
        $f =[];
        $fv = $this->getFace();
        array_push($f, $fv);
        $l = count($f);
        for ($i = 0; $i < $l; $i++) {
            $mj = $i;   

            for ($j = $i+1 ; $j < $l; $j++ ) {
               if ( $f[$j] < $f[$jv] )
               {
                  $mj = $j;  
               }
            }

            $temp = s[$i];
            $s[$i] = $s[$ji];
            $s[$ji] = $temp; 
           
        }
        return $f;
    }

    public function getFace() {  
        $l = count($this->StackCards); 
        $f = [];
        $f[] = $l;
        for($i = 0; $i < $l; $i++) {
            $f[$i] = $StackCards[$i].charAt($StackCards, 0);
        }
        return $f;
    }

     public function pair() {
        $a1;
        $a2; 
        $a3; 
        $a4;

        $f = [];
        $l = count($this->getFace()); 
        $f[] = $l;

        if ( $l != 5 && $this->fourOfAKind() || $this->fullHouse() || $this->threeOfAKind() || $this->twoPair() )
           return false;        //  IF THE HAND IS NOT ONE PAIR      

        $this->sortFace();

        // Checking: a a x y z                           
        $a1 = $f[0] == $f[1];

        // Checking: x a a y z
        $a2 = $f[1] == $f[2];

        // Checking: x y a a z 
        $a3 = $f[2] == $f[3];

        // Checking: x y z a a
        $a4 = $f[3] == $f[4];

        return( $a1 || $a2 || $a3 || $a4 );
    }

    public function twoPair() {
        $a1;
        $a2;
        $a3;

        // ARRAY AND GET COUNT
        $f = [];
        $f [] = $this->getFace();
        $l = count($this->getFace());

        if ( $l != 5 && $this->fourOfAKind() || $this->fullHouse() || $this->threeOfAKind() )
            return false;           

        $f [] = $this->sortFace();

         //CHECKING a b x
	                        
        $a1 = $f[0] == $f[1] &&
           $f[2] == $f[3];

         // CHECKING a x b
        $a2 = $f[0] == $f[1] &&
           $f[3] == $f[4];

        // CHECKING x a a  b b
        $a3 = $f[1] == $f[2] &&
           $f[3] == $f[4] ;

      return( $a1 || $a2 || $a3 );
    }

    public function threeOfAKind() {
        $a1; $a2; $a3;

        // CREATE ARRAY AND GET COUNT
        $f = [];
        $f [] = $this->getFace();
        $l = count($this->getFace());
      
        if ( $l != 5 )
           return false;

        $f [] = $this->sortFace();         // SORT BY FACE FIRST

        // CHECKING  a b x x x
        $a1 = $f[0] == $f[1] &&
              $f[1] == $f[2] &&
              $f[3] != $f[0] &&
              $f[4] != $f[0] &&
              $f[3] != $f[4];

      
        // CHECKING: a x x x b
       $a2 = $f[1] == $f[2] &&
             $f[2] == $f[3] &&
	         $f[0] != $f[1] &&
	         $f[4] != $f[1] &&
	         $f[0] != $f[4];

      
       //  CHECKING  a b x x x
      $a3 = $f[2] == $F[3] &&
            $F[3] == $F[4] &&
	        $F[0] != $F[2] &&
	        $F[1] != $F[2] &&
	        $F[0] != $F[1];

      return( $a1 || $a2 || $a3 );
    }
   
    public function fourOfAKind() {
         $a1; $a2;
    
        // CREATE ARRAY AND GET COUNT
        $f = [];
        $f [] = $this->getFace();
        $l = count($this->getFace());

        if ( $l != 5 )
           return false;

        $f = $this->sortFace();    // SORTING BY FACE

        // CHECKING: 4 cards with the same rank and unmatched card  
        $a1 = $f[0] == $f[1] &&
              $f[1] == $f[2] &&
              $f[2] == $f[3] ;

        // CHECKIN: lower ranked unmatched card + 4 same ranked cards   
       $a2 = $f[1] == $f[2] &&
             $f[2] == $f[3] &&
             $f[3] == $f[4] ;

      return( $a1 || $a2 );
    }

    public function fullHouse() {
         $a1; $a2;
        
        // CREATE ARRAY AND GET COUNT
        $f = [];
        $f [] = $this->getFace();
        $l = count($this->StackCards);

        if ( $l != 5 )
           return false;

        $f = $this->sortFace();      // SORTING FACE FIRST

         //  CHECKING: x x x y y  
        $a1 = $f[0] == $f[1] &&
              $f[1] == $f[2] &&
              $f[3] == $f[4];

        //   CHECKING : x x y y y 
        $a2 = $f[0] == $f[1] &&
              $f[2] == $f[3] &&
              $f[3] == $f[4];

          return( $a1 || $a2 );
        
    }

        // SORT ALL CARDS BT SUIT
    public function flush() {
        $s = [];
        $l = count($this->StackCards);
        if ( $l != 5 )
           return false;   

        $s[]  = $this->sortByMatch(); 
        return( $s[0] == $s[4] );   
    } 

    public function straight() {
        $i; $testFace;
        $f = [];
        $l = count($this->StackCards);
        if ( $l != 5 )
            return false;

        $f = $this->sortFace();          
        if ( $f[4] == 13 )
        {
            $a = $f[0] == 2 && $f[1] == 3 &&
                       $f[2] == 4 && $f[3] == 5 ;
            $b = $f[0] == 10 && $f[1] == 11 &&        
                       $f[2] == 12 && $f[3] == 13 ;

           return ( $a || $b );
        }
        else
        {
           $testFace = f[0] + 1;

           for ( $i = 1; $i < 5; $i++ )
           {
              if ( $f[$i] != $testFace )
                 return false;        // FAILED

              $testFace++;   // NEXT CARD IN THE HAND
           }

           return true;        // FOUND !
        }
    }
    
    public function getMatch(){
        $m = [];
        $l = count($this->StackCards);
        $m[] = $l;
        for($i = 0; $i < $l; $i++)
        {
            $s[$i] = $StackCards[$i].charAt(count($this->StackCards[$i]));
        }
        return $s;
    }
}