<?php

class Pair extends Hand {

    public function __construct($s) {
        super(s);
    }

    public boolean draw(){
        return $this->pair();
    }
    
}