<?php

include_once './Card.php';
include_once './Deck.php';
include_once './Hand.php';
include_once './Straight.php';
include_once './Fullhouse.php';
include_once './FourOfAKind.php';
include_once './Flush.php';
include_once './Pair.php';
include_once './TwoPair.php';


$cards = new Card();
$card = $cards->start();

$deck = new Deck($card);
$hand = new Hand($deck, $card);


// GET USER INPUT
$data = json_decode(file_get_contents("php://input"));
$flush = new Flush($data);
$Straight = new Straight($data);
$pair = new Pair($data);
$twoPair = new TwoPair($data);
$threeOfAKind = new ThreeOfAKind($data);
$fourOfAKind = new FourOfAKind($data);
$fullHouse = new FullHouse($data);

   
    public function autoPlay() {
        $cards = 5;
        $deck->printCards($cards);
        $deck->printDeck();
    
        if ($hand->flush()) {
            echo "Flush";
        }
        
        else if ($hand->straight()) {
            echo "Straight";
        }
        
        else if ($hand->pair()) {
            echo "Pairs";
        }
        
        else if ($hand->twoPair()) {
            echo "Two Pair";
        }
        
        else if ($hand->threeOfAKind()) {
            echo "Three of a Kind";
        }
        
        else if ($hand->fourOfAKind()) {
            echo "Four of a Kind";
        }
        
        else ($hand->fullHouse()) {
            echo "Full House";
        }
    }
    
    public function manualPlay() {
        
        if ($flush->draw()) {
            echo "Flash";
        }
        
        else if ($straight->draw())
        {
            echo "Straight";
        }
        
        else if ($pair->draw())
        {
            echo "Pair";
        }
        
        else if ($twoPair->draw())
        {
            echo "Two Pair";
        }
        
        else if ($threeOfAKind->draw())
        {
            echo "Three of a Kind";
        }
        
        else if ($fourOfAKind->draw())
        {
            echo "Four of a Kind";
        }
        
        else if ($fullHouse->draw())
        {
            echo "Full House";
        }
    }

