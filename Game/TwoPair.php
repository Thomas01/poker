<?php
class TwoPair extends Hand implements Ipoker {

    public function __construct($s) {
        $this->s = $s;
    }

    public function draw() 
    {
        return $this->twoPair();
    }
    
}
