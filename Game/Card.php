<?php

    class Card {
        // Constructor 
        public function __construct( $ranks , $suits,) {
            $this->suits = $suits;
            $this->ranks = $ranks;
        }  
    }

     const $SUITS = ["D", "C", "H", "S"];
     const $RANKS = ["A", "Q", "K", "J", "2", "3", "4", "5", "6", "7", "8", "9", "10"];

    public function start () {
        for ($s = 0; $s < self::SUITS; $s++)
        {
            for ($r = 0; $r < self::RANKS; $r++)
            {
                return new Card($r, $s);
            }
        }
    }