<?php

class FourOfAKind extends Hand {

    
    public function __construct($s) {
        $stack = $this->s = $s;
    }

   // @Override
    public function draw() {
        $a1; $a2;
        $f = [];
        $f [] = $this->getFace();
        $l = count($this->getFace());

        if ( $l != 5 )
            return(false);
        $f = $this->sortFace();     

        // CHECKING: 4 cards of the same rank 
        $a1 = $f[0] == $f[1] &&
             $f[1] == $f[2] &&
             $f[2] == $f[3] ;

       // Check for: lower ranked unmatched card  
        $a2 = $f[1] == $f[2] &&
             $f[2] == $f[3] &&
             $f[3] == $f[4] ;

        return( $a1 || $a2 );
    }

}