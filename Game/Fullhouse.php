<?php
class FullHouse extends Hand {

    public function __construct($s) {
        $this->s = $s;
    }

    //@Override
    public function draw() {
        $a1; $a2;
        // CREATE ARRAY AND GET COUNT
        $f = [];
        $f [] = $this->getFace();
        $l = count($this->stackCards);

        if ( $l != 5 )
           return(false);
        $f = $this->sortByFace();      

          // CHECKING: x x x y y
        $a1 = $f[0] == $f[1] &&
              $f[1] == $f[2] &&
              $f[3] == $f[4];

         //  CHECKING: x x y y y
        $a2 = $f[0] == $f[1] &&
              $f[2] == $f[3] &&
              $f[3] == $f[4];

        return( $a1 || $a2 );
    }
    
}
